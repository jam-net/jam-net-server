# Jam-Net ⇔ Server

### Setup
1. install [deno v1.9.0 or later](https://deno.land/manual@v1.7.5/getting_started/installation)
1. load the dependencies via `deno cache --reload --lock=lock.json src/deps.ts`


### Development
- to start the server, select the appropriate run configuration without or with debugging from
    the Run pane in VS code
- when changing dependencies in `src/deps.ts`, be sure to run the *Update deps lock* task


### Run
`deno run --allow-net src/server.ts`
