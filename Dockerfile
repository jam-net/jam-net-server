FROM hayd/alpine-deno:1.8.0

EXPOSE 8080
EXPOSE 4200

WORKDIR /app

USER deno

# Prepare dependencies
COPY src/deps.ts .
RUN deno cache deps.ts

# Pre-compile app
ADD src .
RUN deno cache server.ts

CMD ["deno", "run", "--allow-net", "server.ts"]
