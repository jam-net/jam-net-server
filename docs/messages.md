# Message-Structure

[[_TOC_]]

## Message Structure

After connection, the client and server sent messages between them to communicate all actions and state changes.

Each message has at least a `type` field, all other fields and how the message is handled is determined by this.


## Shared Types

Some nested types are shared across multiple types.

### Player

| key | value type | value note |
|---|---|---|
| **`id`** | `string` | A stable identifier to relate further messages to this player.<br>Note: The logic for how this id is generated is an implementation detail that should not be relied upon! |
| **`name`** | `string` |  |
| **`color`** | `string` |  |
| **`isHost`** | `boolean` |  |

## Error Responses

Whenever an error occurs, be it due to a wrong request or a server error, a message with the following fields is returned from the Server to the Client:

| key | value type | value note |
|---|---|---|
| **`type`** | `'error'` |  |
| **`code`** | `number` | Identifies the general type of error. |
| **`details`** | `string` | Additional information about the error. |

```json
{ "type":"error", "code":400, "details":"Mo type set" }
```

In this message, the code can be one of the following values:

| error code | error type |
|---|---|
| 400 | Invalid request - due to a missing field |
| 405 | Invalid request - due to a field having a wrong or malformed value |
| 404 | Not found - i.e. a session with the given session id could not be found |
| 403 | Not authorized - the client is not the host and tried to send a message that only the host is entitled to |
| 410 | Gone - i.e. a session with the given session id could be found but is not open to join |
| 428 | Precondition required - the tracked state for the client does not match the pre-condition to handle this message<br>i.e. trying to play before a session was joined |
| 500 | Server-side error during handling the last message. |

<br><br>

<!-- Message format template:
| key | value type | value note |
|---|---|---|
| **`type`** | `''` |  |
| **``** | `` |  |
-->

## Client to Server Messages

### create

Creates a new session and sets the client as its host.
**Server-Response**: [created](#created)

| key | value type | value note |
|---|---|---|
| **`type`** | `'create'` |  |
| **`gameId`** | `string` | A game identifier the client can freely set.<br>Use it to ensure that the entered session id is of a session the client can actually handle. |
| **`name`** | `string` | The player's name.<br>*Optional*, defaults to `anonymous`. |
| **`color`** | `string` | The player's color to display i.e. their name in.<br>*Optional*, defaults to `#CCCCCC`. |

```json
{ "type":"create", "gameId":"tic-tac-toe", "name":"Godot", "color":"#008040" }
```

### join

Joins an existing session.

**Server-Response**: [joined](#joined)

| key | value type | value note |
|---|---|---|
| **`type`** | `'join'` |  |
| **`gameId`** | `string` | A game identifier the client can freely set.<br>Use it to ensure that the entered session id is of a session the client can actually handle. |
| **`sessionId`** | `string` | The id of a session previously created by another client. |
| **`name`** | `string` | The player's name.<br>*Optional*, defaults to `anonymous`. |
| **`color`** | `string` | The player's color to display i.e. their name in.<br>*Optional*, defaults to `#CCCCCC`. |

```json
{ "type":"join", "gameId":"tic-tac-toe", "sessionId":"JAM1NET", "name":"Godot", "color":"#008040" }
```

### start

Starts a previously created session. This allows clients to sent play commands and prevents further clients from joining the session.

> :warning: **only permitted by the host player**

**Server-Response**: [started](#started)

| key | value type | value note |
|---|---|---|
| **`type`** | `'start'` |  |

```json
{ "type":"start" }
```

<br><br>

## Server to Client Messages

### created

A new session was created based on a previous `create` message.

| key | value type | value note |
|---|---|---|
| **`type`** | `'created'` |  |
| **`sessionId`** | `string` |  |

### joined

An existing session was joined based on a previous `join` message.

| key | value type | value note |
|---|---|---|
| **`type`** | `'joined'` |  |
| **`players`** | `Player[]` | see [Shared Types](#shared-types) |

### peerJoined

Another player joined the session which this client previously joined.

| key | value type | value note |
|---|---|---|
| **`type`** | `'peerJoined'` |  |
| **`player`** | `Player` | see [Shared Types](#shared-types) |

### started

The session which this client previously joined was marked as having started by the host client.

| key | value type | value note |
|---|---|---|
| **`type`** | `'started'` |  |

### peerLeft

Another player left the session which this client previously joined due to as disconnect.

| key | value type | value note |
|---|---|---|
| **`type`** | `'peerJoined'` |  |
| **`playerId`** | `string` |  |

### ended

The session which this client previously joined ended, i.e. due to host ending it or all other players disconnecting.

| key | value type | value note |
|---|---|---|
| **`type`** | `'ended'` |  |
