import { cryptoRandomString, WebSocket } from "./deps.ts";

// Types:

export interface Session {
  gameId: string;
  isOpenToJoin: boolean;
  players: PlayerData[];
}

export type SessionId = string & { _t: "SessionId" };

export type PlayerId = string & { _t: "PlayerId" };

export interface PublicPlayerData {
  id: PlayerId;
  name: string;
  color: string;
  isHost: boolean;
}

export interface PlayerData extends PublicPlayerData {
  socket: WebSocket;
}

// Store:

export class SessionStore {
  private static sessionDict: { [key: string]: Session } = {};

  static getStats() {
    return {
      totalSessions: Object.keys(this.sessionDict).length,
      totalUsers: Object.values(this.sessionDict).reduce(
        (acc, curr) => acc + curr.players.length,
        0,
      ),
    };
  }

  static getIsSessionKnown(sessionId: SessionId): boolean {
    return this.sessionDict[sessionId] !== undefined;
  }

  static validateIsPlayerInSession(
    sessionId: SessionId,
    playerRid: number,
  ): PlayerId | null {
    return this.sessionDict[sessionId]?.players.find((player) =>
      player.socket.conn.rid === playerRid
    )?.id ?? null;
  }

  static validateIsPlayerHostForSession(
    sessionId: SessionId,
    playerRid: number,
  ): PlayerId | null {
    return this.sessionDict[sessionId]?.players.find((player) =>
      player.socket.conn.rid === playerRid && player.isHost
    )?.id ?? null;
  }

  static getIsSessionOfGame(sessionId: SessionId, gameId: string): boolean {
    return this.sessionDict[sessionId]?.gameId === gameId;
  }

  static getIsSessionOpenToJoin(sessionId: SessionId): boolean {
    return this.sessionDict[sessionId]?.isOpenToJoin ?? false;
  }

  static getPlayerSocketsForSession(
    sessionId: SessionId,
    excludePlayerRid: number | null,
  ): WebSocket[] {
    let players = this.sessionDict[sessionId]?.players;
    if (excludePlayerRid) {
      players = players.filter((player) =>
        player.socket.conn.rid !== excludePlayerRid
      );
    }
    return players.map((player) => player.socket) ?? [];
  }

  static removePlayerForSocket(socket: WebSocket) {
    let affectedSessionId: SessionId | null = null;
    let affectedPlayerId: PlayerId | null = null;
    const affectedRid = socket.conn.rid;

    Object.entries(this.sessionDict).forEach(([id, session]) => {
      const affectedIndex = session.players.findIndex((player) =>
        player.socket.conn.rid === affectedRid
      );
      if (affectedIndex !== -1) {
        affectedSessionId = id as SessionId;
        affectedPlayerId = session.players[affectedIndex].id;
        session.players.splice(affectedIndex, 1);
      }
    });

    if (affectedSessionId === null || affectedPlayerId === null) {
      return null;
    } else {
      return {
        sessionId: affectedSessionId,
        playerId: affectedPlayerId,
      };
    }
  }

  static createAndJoin(
    socket: WebSocket,
    gameId: string,
    name: string,
    color: string,
  ) {
    let sessionId: SessionId;
    do {
      sessionId = cryptoRandomString({
        length: 7,
        type: "distinguishable",
      });
    } while (this.sessionDict[sessionId]);

    this.sessionDict[sessionId] = {
      gameId,
      isOpenToJoin: true,
      players: [{
        id: "player0" as PlayerId,
        socket,
        name,
        color,
        isHost: true,
      }],
    };
    return { sessionId };
  }

  static join(
    socket: WebSocket,
    sessionId: SessionId,
    name: string,
    color: string,
  ) {
    const session = this.sessionDict[sessionId];
    if (!session) {
      throw Error("Session not found");
    }
    if (!session.isOpenToJoin) {
      throw Error("Session is not open to join");
    }

    const id = `player${session.players.length}` as PlayerId;

    session.players.push({
      id,
      socket,
      name,
      color,
      isHost: false,
    });

    return {
      id: id,
      players: session.players.map((player) => ({
        id: player.id,
        name: player.name,
        color: player.color,
        isHost: player.isHost,
      })),
    };
  }

  static setSessionToClosed(sessionId: SessionId): void {
    const session = this.sessionDict[sessionId];
    if (session) {
      session.isOpenToJoin = false;
    }
  }
}
