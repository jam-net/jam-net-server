import { httpServer } from "./http/server.ts";
import { wsServer } from "./ws/server.ts";

if (import.meta.main) {
  console.log("main - Start");

  Promise.race([
    httpServer(),
    wsServer(),
  ]).finally(() => {
    console.log("main - Exit");
  });
}
