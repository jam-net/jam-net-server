import { serve, ServerRequest } from "../deps.ts";
import { SessionStore } from "../store.ts";

export async function httpServer() {
  const port = +Deno.args[0] || 4200;
  console.log(
    `HTTP - Start - listening for requests on :${port}`,
  );

  for await (const req of serve({ port })) {
    const url = req.url.replace(/\/$/, "");
    console.log(`HTTP - Request - url: ${url}`);

    switch (url) {
      case "/health":
        jsonResponse(req, { "status": "OK" });
        break;
      case "/stats":
        jsonResponse(req, SessionStore.getStats());
        break;
      default:
        homepageResponse(req);
    }
  }
}

function jsonResponse(req: ServerRequest, body: unknown) {
  req.respond({
    "body": JSON.stringify(body),
    "headers": new Headers({
      "content-type": "application/json",
      "cache-control": "no-store",
    }),
  });
}

function homepageResponse(req: ServerRequest) {
  req.respond({
    "body": `
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Jam-Net ⇔ Server</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
  <link rel="stylesheet" href="https://unpkg.com/bulmaswatch/slate/bulmaswatch.min.css">
  <style>
    html {
      overflow-y: auto;
    }

    * {
      transition: all 0s !important;
    }
  </style>
</head>
<body>
  <section class="hero is-primary" style="background-color: #004D40;">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
          Jam-Net ⇔ Server
        </h1>
        <span class="subtitle">
          Because no one has time to implement multiplayer for a Game Jam.
        </span>
      </div>
    </div>
  </section>
  <section class="section container pb-0">
    <h2 class="subtitle is-4">About</h2>
    <p>
      Jam-Net is a generic Server and Client libraries to speed through adding multiplayer to your games.
    </p>
    <p>
      It includes matchmaking and networking between clients over a central server.
    </p>
    <div class="buttons mt-4">
      <a class="button is-outlined is-small" href="https://gitlab.com/jam-net"> Learn more </a>
    </div>
  </section>
  <section class="section container">
    <h2 class="subtitle is-4">Status</h2>
    <div class="buttons">
      <a class="button is-outlined is-small" href="/stats"> open Statistics </a>
    </div>
  </section>
</body>
</html>
    `,
    headers: new Headers({
      "content-type": "text/html; charset=utf-8",
    }),
  });
}
