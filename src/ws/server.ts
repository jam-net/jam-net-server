import { acceptWebSocket, serve } from "../deps.ts";

import { handleWsConnection } from "./connection-handler.ts";

export async function wsServer() {
  const port = Deno.args[1] || "8080";
  console.log(
    `WS - Start - listening for WebSocket connections on :${port}`,
  );

  for await (const req of serve(`:${port}`)) {
    const { conn, r: bufReader, w: bufWriter, headers } = req;
    acceptWebSocket({
      conn,
      bufReader,
      bufWriter,
      headers,
    })
      .then(handleWsConnection)
      // deno-lint-ignore no-explicit-any
      .catch(async (err: any) => {
        console.error(`Server - Error - failed to accept websocket: ${err}`);
        await req.respond({ status: err.code ?? 400 });
      });
  }
}
