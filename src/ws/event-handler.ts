import { WebSocket } from "../deps.ts";
import { SessionId, SessionStore } from "../store.ts";

import {
  sendError,
  sendMessage,
  sendMessageForPlayers,
  SimpleObject,
  validateAsHost,
} from "./utils.ts";

// Top-Level Event Handlers:

export async function handleEvent(
  socket: WebSocket,
  type: string,
  args: SimpleObject,
  state: SimpleObject,
) {
  switch (type) {
    case "create":
      return handleCreate(socket, args);
    case "join":
      return handleJoin(socket, args);
    case "start":
      return handleStart(socket, args, state);
    default:
      console.log(
        `Handler - Event - Invalid type received: ${type}`,
      );
      await sendError(socket, 405, `Invalid type sent: ${type}`);
      return {};
  }
}

export function handleClose(socket: WebSocket) {
  const leaveData = SessionStore.removePlayerForSocket(socket);
  if (leaveData) {
    if (leaveData.playerId === 0) {
      return sendMessageForPlayers(leaveData.sessionId, null, "ended", {});
    } else {
      return sendMessageForPlayers(leaveData.sessionId, null, "peerLeft", {
        playerId: leaveData.playerId,
      });
    }
  }
}

// Detailed Event Handlers:

async function handleCreate(socket: WebSocket, args: SimpleObject) {
  const gameId = args.gameId?.toString();
  if (!gameId) {
    return sendError(socket, 400, "No gameId set");
  }

  const { sessionId } = SessionStore.createAndJoin(
    socket,
    gameId,
    args.name?.toString() ?? "anonymous",
    args.color?.toString() ?? "#CCCCCC",
  );
  await sendMessage(socket, "created", { sessionId });

  return { sessionId };
}

async function handleJoin(socket: WebSocket, args: SimpleObject) {
  const sessionId = args.sessionId?.toString() as SessionId ?? null;
  if (!sessionId) {
    return sendError(socket, 400, "No sessionId set");
  }

  if (!SessionStore.getIsSessionKnown(sessionId)) {
    return sendError(
      socket,
      404,
      `Session with sessionId ${sessionId} not found`,
    );
  }

  const gameId = args.gameId?.toString();
  if (!gameId) {
    return sendError(socket, 400, "No gameId set");
  }
  if (!SessionStore.getIsSessionOfGame(sessionId, gameId)) {
    return sendError(
      socket,
      404,
      `Session with sessionId ${sessionId} is not of game ${gameId}`,
    );
  }
  if (!SessionStore.getIsSessionOpenToJoin(sessionId)) {
    return sendError(
      socket,
      410,
      `Session with sessionId ${sessionId} is not open to join`,
    );
  }

  const name = args.name?.toString() ?? "anonymous";
  const color = args.color?.toString() ?? "#CCCCCC";

  const { id, players } = SessionStore.join(
    socket,
    sessionId,
    name,
    color,
  );
  await sendMessage(socket, "joined", { players });

  await sendMessageForPlayers(sessionId, socket.conn.rid, "peerJoined", {
    player: {
      id: id,
      name: name,
      color: color,
      isHost: false,
    },
  });

  return { sessionId };
}

async function handleStart(
  socket: WebSocket,
  _args: SimpleObject,
  state: SimpleObject,
) {
  let sessionId: SessionId;
  try {
    sessionId = (await validateAsHost(socket, state)).sessionId;
  } catch {
    return;
  }

  SessionStore.setSessionToClosed(sessionId);

  await sendMessageForPlayers(sessionId, null, "started", {});
}
