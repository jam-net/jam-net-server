import { WebSocket } from "../deps.ts";

import { SessionId, SessionStore } from "../store.ts";

export interface SimpleObject {
  [key: string]:
    | string
    | number
    | boolean
    | null
    | SimpleObject
    | SimpleObject[];
}

// Validators:

export async function validateJoined(
  socket: WebSocket,
  state: SimpleObject,
) {
  const sessionId = state.sessionId?.toString() as SessionId;
  if (!sessionId) {
    await sendError(
      socket,
      428,
      `No session joined for this connection`,
    );
    throw Error("No session joined");
  }

  const playerId = SessionStore.validateIsPlayerInSession(
    sessionId,
    socket.conn.rid,
  );
  if (!playerId) {
    await sendError(
      socket,
      403,
      "Player not found in saved session",
    );
    throw Error("Player not set in session");
  }

  return { sessionId, playerId };
}

export async function validateAsHost(
  socket: WebSocket,
  state: SimpleObject,
) {
  const sessionId = state.sessionId?.toString() as SessionId;
  if (!sessionId) {
    await sendError(
      socket,
      428,
      "No session joined for this connection",
    );
    throw Error("No session joined");
  }

  const playerId = SessionStore.validateIsPlayerHostForSession(
    sessionId,
    socket.conn.rid,
  );
  if (!playerId) {
    await sendError(
      socket,
      403,
      "Player not set as host for saved session",
    );
    throw Error("Not set as host for session");
  }

  return { sessionId, playerId };
}

// WS send functions:

export async function sendMessageForPlayers(
  sessionId: SessionId,
  excludePlayerRid: number | null,
  type: string,
  args: SimpleObject,
) {
  const data = JSON.stringify({ ...args, type });
  const peerSocketList = SessionStore.getPlayerSocketsForSession(
    sessionId,
    excludePlayerRid,
  );
  for (const peerSocket of peerSocketList) {
    await peerSocket.send(data);
  }
}

export function sendMessage(
  socket: WebSocket,
  type: string,
  args: SimpleObject,
) {
  const data = JSON.stringify({ ...args, type });
  return socket.send(data);
}

export function sendError(socket: WebSocket, code: number, details: string) {
  const data = JSON.stringify({ type: "error", code, details });
  return socket.send(data);
}
