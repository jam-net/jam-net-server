import { isWebSocketCloseEvent, WebSocket } from "../deps.ts";

import { handleClose, handleEvent } from "./event-handler.ts";
import { sendError, SimpleObject } from "./utils.ts";

export async function handleWsConnection(socket: WebSocket) {
  console.log(`WS - Connect - rid: ${socket.conn.rid}`);

  try {
    let state: SimpleObject = {};

    for await (const rawEvent of socket) {
      let eventData: string | null = null;
      if (rawEvent instanceof Uint8Array) {
        eventData = new TextDecoder().decode(rawEvent);
      }
      if (typeof rawEvent === "string") {
        eventData = rawEvent;
      }

      if (eventData) {
        const { type, ...args } = JSON.parse(eventData);
        if (!type) {
          console.error(
            `WS - Event - rid: ${socket.conn.rid} - missing type`,
          );
          await sendError(socket, 400, "Mo type set");
          continue;
        }
        console.log(
          `WS - Event - rid: ${socket.conn.rid} - type: ${type}`,
        );
        const stateChange = await handleEvent(socket, type, args, state);
        if (stateChange) {
          state = { ...state, ...stateChange };
        }
        continue;
      }

      if (isWebSocketCloseEvent(rawEvent)) {
        const { code } = rawEvent;
        console.log(
          `WS - Close - rid: ${socket.conn.rid} - code=${code ?? "n/a"}`,
        );
        await handleClose(socket);
        state = {};
      }
    }
  } catch (err) {
    console.error(
      `WS - Error - failed to handle frame: ${err} - closing connection`,
    );
    await sendError(socket, 500, `Failed to handle frame: ${err}`);

    if (!socket.isClosed) {
      await socket.closeForce();
    }
  }
}
